# Git2WP

Write articles in Markdown format and auto post to your wordpress site( uses GitLab-CI ).

## Install

### WordPress
	
Install Jetpack then enable "Settings" -> "Writing" -> "Write posts or pages in plain-text Markdown syntax".

Disable "Settings" -> "Media" -> "Organize my uploads into month- and year-based folders" so you can use pictures more easily.

### Local

	$ git clone git@gitlab.com:haway/git2wp.git
	$ cp configs.txt.example configs.txt
	$ vi configs.txt
	(change your website, username and password)
	$ git add post_rpc.py .gitlab-ci.yml configs.txt	

## Usage

1. Write your posts in Markdown format.
2. Add it to git.
3. push it to GitLab.

## configs.txt

It's a json format file, Change 'website' and 'username' and 'password' in configs.txt.

Setup "typo-enable" to 1 if you want post_rpc.py to replace typo words to correct.

Put typos in configs.txt file in "typo" section, like `error`:`correct`

Example:

	{                                                                               
    	"website": "https://yourdomain",
    	"username": "youraccount",
    	"password": "123456",
    	"typo-enable": 1,
    	"typo": {
        	"usermame":"username",
        	"posr":"post",
        	"word":"words",
        	"chrck":"check",
        	"資要":"資料",
        	"些者":"接者",
        	"為射麼":"為什麼",
        	"只的":"指的"
    	}
	}


## Example

	$ git pull git@gitlab.com:haway/git2wp.git
	$ cp configs.txt.example configs.txt
	$ vi configs.txt
	$ git add post_rpc.py .gitlab-ci.yml configs.txt	
	$ git add (your markdown article)
	$ git commit -m 'messages'
	$ git push

